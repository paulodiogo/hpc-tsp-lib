﻿var Grafico = function () {

    var $plotContainer = $("#ploted");

    var construirGrafico = function (json) {
        var data = new google.visualization.DataTable();
        data.addColumn('number', 'x');


        for (var i = 0; i < json.routes.length; i++) {
            data.addColumn('number', json.routes[i].nome);
            data.addColumn({
                type: 'string',
                role: 'tooltip'
            });
            data.addColumn({
                type: 'boolean',
                role: 'certainty'
            });
        }


        for (var i = 0; i < json.rows.length; i++) {
            data.addRow(json.rows[i]);
        }

        var options = {
            title: json.instance + " Cost = " + json.distance,
            width: 700,
            height: 600,
            vAxis: {
                viewWindow: json.minMax
            },
            hAxis: {
                viewWindow: json.minMax
            },
            legend: 'true',
            annotationsWidth: 5,
            pointSize: 5
        };

        var id = "chart" + new Date().getTime();
        $plotContainer.append($("<div />").prop("id", id));

        var chart = new google.visualization.LineChart(document.getElementById(id));
        chart.draw(data, options);
    }


    return {

        GerarGraficos: function () {

            Grafico.ExecutarConstrutivo();

        },
        ExecutarConstrutivo: function () {
            $.get($("#url").val(), { instancia: $("#instancia").val(), algoritmo: $("#construtivo").val() })
            .done(function (json) {
                construirGrafico(json);

                if ($("#buscaLocal").val().length) {
                    Grafico.ExecutarBuscaLocal();
                }
            });            
        },
        ExecutarBuscaLocal: function () {
            $.get($("#urlBuscaLocal").val(), { instancia: $("#instancia").val(), algoritmoConstrutivo: $("#construtivo").val(), algoritmoBuscaLocal: $("#buscaLocal").val() })
            .done(function (json) {
                construirGrafico(json);

                if ($("#metaHeuristica").val().length) {
                    Grafico.ExecutarMetaHeuristica();
                }
            });
        },
        ExecutarMetaHeuristica: function () {
            $.get($("#urlMetaHeuristica").val(), {
                instancia: $("#instancia").val(),
                algoritmoConstrutivo: $("#construtivo").val(),
                algoritmoBuscaLocal: $("#buscaLocal").val(),
                algoritmoMetaHeuristica: $("#metaHeuristica").val()
            })
            .done(function (json) {
                construirGrafico(json);
            });
        }

    }


}();