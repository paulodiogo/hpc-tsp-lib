﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Trabalho.Domain;
using TspLibNet;
using TspLibNet.Graph.Nodes;
using TspLibNet.TSP;
using TspLibNet.TSP.Defines;

namespace Trabalho.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult ExecutarConstrutivo(string instancia, AlgoritmoConstrutivo algoritmo)
        {

            var tspFile = TspFile.Load(Server.MapPath("~/Instances/" + instancia + ".vrp"));
            var problem = FromTspFile(tspFile);
            ISolver solver = null;

            switch (algoritmo)
            {
                case AlgoritmoConstrutivo.NN:
                    solver = new NearestNeighbor(tspFile, problem);
                    break;
                case AlgoritmoConstrutivo.CW:
                    solver = new ParallelClarkeWright(tspFile, problem);
                    break;
            }

            var solution = solver.Solve();

            var distance = TourDistance(problem, solution.Tours, solution.Depot);

            var resultado = BuildResult(solution, instancia, distance);

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ExecutarBuscaLocal(string instancia, AlgoritmoConstrutivo algoritmoConstrutivo, AlgoritmoBuscaLocal algoritmoBuscaLocal)
        {
            var tspFile = TspFile.Load(Server.MapPath("~/Instances/" + instancia + ".vrp"));
            var problem = FromTspFile(tspFile);

            ISolver solveConstrutivo = null;
            switch (algoritmoConstrutivo)
            {
                case AlgoritmoConstrutivo.NN:
                    solveConstrutivo = new NearestNeighbor(tspFile, problem);
                    break;
                case AlgoritmoConstrutivo.CW:
                    solveConstrutivo = new ParallelClarkeWright(tspFile, problem);
                    break;
            }

            var solucaoConstrutivo = solveConstrutivo.Solve();

            var solverBuscaLocal = new TwoExchange(tspFile, problem, solucaoConstrutivo.Tours);

            var solution = solverBuscaLocal.Solve();

            var distance = TourDistance(problem, solution.Tours, solution.Depot);

            var resultado = BuildResult(solution, instancia, distance);

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }


        public JsonResult ExecutarMetaHeuristica(string instancia,
            AlgoritmoConstrutivo algoritmoConstrutivo,
            AlgoritmoBuscaLocal algoritmoBuscaLocal,
            AlgoritmoMetaHeuristica algoritmoMetaHeuristica)
        {
            var tspFile = TspFile.Load(Server.MapPath("~/Instances/" + instancia + ".vrp"));
            var problem = FromTspFile(tspFile);

            ISolver solveConstrutivo = null;
            switch (algoritmoConstrutivo)
            {
                case AlgoritmoConstrutivo.NN:
                    solveConstrutivo = new NearestNeighbor(tspFile, problem);
                    break;
                case AlgoritmoConstrutivo.CW:
                    solveConstrutivo = new ParallelClarkeWright(tspFile, problem);
                    break;
            }

            var solucaoConstrutivo = solveConstrutivo.Solve();

            var solverBuscaLocal = new TwoExchange(tspFile, problem, solucaoConstrutivo.Tours);

            var solutionBuscaLocal = solverBuscaLocal.Solve();

            var solverMetaHeuristica = new TabuSearch(tspFile, problem, solutionBuscaLocal.Tours);

            var solution = solverMetaHeuristica.Solve();

            var distance = TourDistance(problem, solution.Tours, solution.Depot);

            var resultado = BuildResult(solution, instancia, distance);

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        private dynamic BuildResult(ISolution solution, string instancia, double distance = 0d)
        {
            var rows = new List<object[]>();

            var item = solution.Tours;
            var quantidadeColunas = (item.Count() * 3) + 1;
            var colunasGrafico = new object[quantidadeColunas];
            var offset = 1;
            var depot = solution.Depot as Node2D;
            var aux = 1;

            foreach (var rota in solution.Tours)
            {

                colunasGrafico[0] = depot.X;
                colunasGrafico[offset] = depot.Y;
                colunasGrafico[offset + 1] = string.Format("Route #{0}", aux);
                colunasGrafico[offset + 2] = false;

                rows.Add(colunasGrafico.Select(x => x == null ? null : x).ToArray());
                colunasGrafico = new object[quantidadeColunas];

                foreach (Node2D ponto in rota)
                {
                    colunasGrafico[0] = ponto.X;
                    colunasGrafico[offset] = ponto.Y;
                    colunasGrafico[offset + 1] = string.Format("Route #{0} - Costumer {1}", aux, ponto.Id);
                    colunasGrafico[offset + 2] = true;

                    rows.Add(colunasGrafico.Select(x => x == null ? null : x).ToArray());
                    colunasGrafico = new object[quantidadeColunas];
                }

                colunasGrafico[0] = depot.X;
                colunasGrafico[offset] = depot.Y;
                colunasGrafico[offset + 1] = string.Format("Route #{0}", aux);
                colunasGrafico[offset + 2] = false;

                rows.Add(colunasGrafico.Select(x => x == null ? null : x).ToArray());
                colunasGrafico = new object[quantidadeColunas];
                offset += 3;
                aux++;
            }

            var resultado = new
            {
                routes = solution.Tours.Select(x => new
                {
                    nome = "Route #" + (solution.Tours.ToList().FindIndex(y => y == x) + 1)
                }),
                rows = rows,
                instance = instancia,
                minMax = 1000,
                distance = distance

            };

            return resultado;
        }

        private double TourDistance(IProblem problem, IEnumerable<IEnumerable<INode>> tours, INode depot)
        {
            double distance = 0;

            foreach (var item in tours)
            {
                distance += TourDistance(problem, item, depot);
            }

            return distance;
        }

        private double TourDistance(IProblem problem, IEnumerable<INode> tour, INode depot)
        {
            double distance = problem.EdgeWeightsProvider.GetWeight(depot, tour.First());

            for (int i = 0; i + 1 < tour.Count(); i++)
            {
                INode first = i == 0 ? tour.First() : tour.ElementAt(i);
                INode second = tour.ElementAt(i + 1);
                double weight = problem.EdgeWeightsProvider.GetWeight(first, second);
                distance += weight;
            }

            distance += problem.EdgeWeightsProvider.GetWeight(tour.Last(), depot);

            return distance;
        }

        private static CapacitatedVehicleRoutingProblem FromTspFile(TspFile tspFile)
        {
            if (tspFile.Type != FileType.CVRP)
            {
                throw new ArgumentOutOfRangeException("tspFile");
            }

            TspFileDataExtractor extractor = new TspFileDataExtractor(tspFile);
            var nodeProvider = extractor.MakeNodeProvider();
            var nodes = nodeProvider.GetNodes();
            var edgeProvider = extractor.MakeEdgeProvider(nodes);
            var edgeWeightsProvider = extractor.MakeEdgeWeightsProvider();
            var fixedEdgesProvider = extractor.MakeFixedEdgesProvider(nodes);
            var depots = extractor.MakeDepotsProvider(nodes);
            var demand = extractor.MakeDemandProvider(nodes);
            return new CapacitatedVehicleRoutingProblem(tspFile.Name, tspFile.Comment, nodeProvider, edgeProvider, edgeWeightsProvider, fixedEdgesProvider, depots, demand);
        }
    }

    public enum AlgoritmoConstrutivo
    {
        NN = 0,
        CW
    }

    public enum AlgoritmoBuscaLocal
    {
        E2X = 0
    }

    public enum AlgoritmoMetaHeuristica
    {
        BT = 0
    }
}