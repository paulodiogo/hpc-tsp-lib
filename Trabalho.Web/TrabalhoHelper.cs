﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Trabalho.Web
{
    public static class TrabalhoHelper
    {

        public static HtmlString SelectInstancias(this HtmlHelper helper)
        {
            var resultado = new TagBuilder("select");
            resultado.AddCssClass("form-control");
            resultado.MergeAttribute("id", "instancia");

            var path = System.Web.HttpContext.Current.Server.MapPath(@"~/Instances/");

            var nomesArquivos = Directory.GetFiles(path, "*.vrp");

            foreach (var item in nomesArquivos)
            {
                var valor = Path.GetFileNameWithoutExtension(item);

                var option = new TagBuilder("option");
                option.MergeAttribute("value", valor);
                option.InnerHtml = valor;
                resultado.InnerHtml += option.ToString();
            }

            return MvcHtmlString.Create(resultado.ToString());
        }

    }
}