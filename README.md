## Trabalho HPC

Para rodar os projetos é necessário instalar o  [Visual Studio Community](https://www.visualstudio.com/pt-br/products/visual-studio-community-vs.aspx) 

## Heurística implementadas
- Nearest Neighbor
- Clarke & Wright (Sequential e Parallel)
- 2-opt
- Tabu Search

## Projetos

1. Trabalho.Apresentacao
	* Projeto console, rodas todos os heuristicas
2. Trabalho.Domain
	* Projeto com as implementações
3. Trabalho.Web
	* Implementação Web do projeto



