﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trabalho.Domain;
using TspLibNet;

namespace Trabalho.Apresentacao
{
    public class Resultado
    {


        public Resultado(string instancia, double custo, int quantidadeRotas, TimeSpan tempo, IEnumerable<Improvement> improvements = null)
        {
            this.Instancia = instancia;
            this.QuantidadeRotas = quantidadeRotas;
            this.Custo = custo;
            this.Tempo = tempo;
            this.Improvements = improvements ?? Enumerable.Empty<Improvement>();
        }

        public int QuantidadeRotas { get; private set; }
        public string Instancia { get; private set; }
        public double Custo { get; private set; }
        public TimeSpan Tempo { get; private set; }
        public System.Collections.Generic.IEnumerable<Improvement> Improvements { get; private set; }

        public override string ToString()
        {
            return string.Format("{0};{1};{2};{3};", this.Instancia.Split(new string[] { "/", "." }, StringSplitOptions.RemoveEmptyEntries)[1], this.Custo, this.QuantidadeRotas, this.Tempo.ToString());
        }

    }
}
