﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trabalho.Domain;
using TspLibNet;
using TspLibNet.Graph.Nodes;
using TspLibNet.Tours;
using TspLibNet.TSP;
using TspLibNet.TSP.Defines;

namespace Trabalho.Apresentacao
{
    class Program
    {
        static void Main(string[] args)
        {

            string[] arguments = Environment.GetCommandLineArgs();

            var stopWhatch = new Stopwatch();
            var resultados = new List<Resultado>();
            var resultados2 = new List<Resultado>();
            var resultados3 = new List<Resultado>();
            var resultados4 = new List<Resultado>();
            var resultados5 = new List<Resultado>();
            var resultados6 = new List<Resultado>();
            var resultados7 = new List<Resultado>();


            var tours = new Dictionary<Node2D, IEnumerable<IEnumerable<TspLibNet.Graph.Nodes.INode>>>();


            var nomesArquivos = Directory.GetFiles(@"Instances/", "*.vrp");
            foreach (var item in nomesArquivos)
            {
                var tspFile = TspFile.Load(item);
                var problem = FromTspFile(tspFile);

                var solution = RodarNN(stopWhatch, resultados, item, tspFile, problem);

                var solution2 = RodarCW(stopWhatch, resultados2, tours, item, tspFile, problem);

                var solution3 = Rodar2Opt(stopWhatch, resultados3, item, tspFile, problem, solution);

                var solution4 = Rodar2Opt(stopWhatch, resultados4, item, tspFile, problem, solution2);

                RodarTabu(stopWhatch, resultados5, item, tspFile, problem, solution3);
                RodarTabu(stopWhatch, resultados6, item, tspFile, problem, solution4);

            }

            GravarResultado(resultados, "NearestNeighbor");
            GravarResultado(resultados2, "ClarkeWright");
            GravarResultado(resultados3, "TwoExchange-NN");
            GravarResultado(resultados4, "TwoExchange-CW");
            GravarResultado(resultados5, "TabuSearch-IS-TwoExchange-NN");
            GravarResultado(resultados6, "TabuSearch-IS-TwoExchange-CW");

            Console.WriteLine("Construtivos: Fim");
            Console.WriteLine("Pressione qualquer tecla para finalizar...");

            Console.ReadKey();

        }

        private static ISolution RodarNN(Stopwatch stopWhatch, List<Resultado> resultados, string item, TspFile tspFile, CapacitatedVehicleRoutingProblem problem)
        {
            var nearestNeighbor = new NearestNeighbor(tspFile, problem);

            stopWhatch.Restart();
            var solution = nearestNeighbor.Solve();
            stopWhatch.Stop();

            var totalCost = TourDistance(problem, solution.Tours, solution.Depot);
            resultados.Add(new Resultado(item, totalCost, solution.Tours.Count(), stopWhatch.Elapsed));
            Console.WriteLine("NearestNeighbor: " + item + " => " + stopWhatch.Elapsed);
            return solution;
        }

        private static void RodarTabu(Stopwatch stopWhatch, List<Resultado> resultados4, string item, TspFile tspFile, CapacitatedVehicleRoutingProblem problem, ISolution solution3)
        {
            var tabuSearch = new TabuSearch(tspFile, problem, solution3.Tours);

            stopWhatch.Restart();
            var solution4 = tabuSearch.Solve();
            stopWhatch.Stop();

            var totalCost4 = TourDistance(problem, solution4.Tours, solution4.Depot);
            resultados4.Add(new Resultado(item, totalCost4, solution4.Tours.Count(), stopWhatch.Elapsed, (solution4 as ISolutionTabu).Improvements));

            Console.WriteLine("TabuSearch: " + item + " => " + stopWhatch.Elapsed);
        }

        private static ISolution Rodar2Opt(Stopwatch stopWhatch, List<Resultado> resultados3, string item, TspFile tspFile, CapacitatedVehicleRoutingProblem problem, ISolution solution2)
        {
            var twoExchange = new TwoExchange(tspFile, problem, solution2.Tours);

            stopWhatch.Restart();
            var solution3 = twoExchange.Solve();
            stopWhatch.Stop();

            var totalCost3 = TourDistance(problem, solution3.Tours, solution3.Depot);
            resultados3.Add(new Resultado(item, totalCost3, solution3.Tours.Count(), stopWhatch.Elapsed));

            Console.WriteLine("TwoExchange: " + item + " => " + stopWhatch.Elapsed);
            return solution3;
        }

        private static ISolution RodarCW(Stopwatch stopWhatch, List<Resultado> resultados2, Dictionary<Node2D, IEnumerable<IEnumerable<INode>>> tours, string item, TspFile tspFile, CapacitatedVehicleRoutingProblem problem)
        {
            var clarkeWright = new ParallelClarkeWright(tspFile, problem);

            stopWhatch.Restart();
            var solution2 = clarkeWright.Solve();
            stopWhatch.Stop();

            var totalCost2 = TourDistance(problem, solution2.Tours, solution2.Depot);
            resultados2.Add(new Resultado(item, totalCost2, solution2.Tours.Count(), stopWhatch.Elapsed));
            tours.Add(solution2.Depot as Node2D, solution2.Tours);

            Console.WriteLine("ClarkeWright: " + item + " => " + stopWhatch.Elapsed);
            return solution2;
        }

        private static void GravarResultado(IEnumerable<Resultado> resultados, string name)
        {
            var agora = DateTime.Now;
            var fileName = string.Format("./{0}-{1}-{2}-{3}-{4}-{5}-{6}.txt", name, agora.Year, agora.Month, agora.Day, agora.Hour, agora.Minute, agora.Second);
            
            using (FileStream fs = new FileStream(fileName, FileMode.Append, FileAccess.Write))
            using (StreamWriter sw = new StreamWriter(fs))
            {
                foreach (var item in resultados)
                {
                    var tspFile = TspFile.Load(item.Instancia);
                    var problem = FromTspFile(tspFile);

                    sw.WriteLine(item.ToString());

                    if (item.Improvements.Any())
                    {
                        GravarImprovements(problem, item, agora, name);
                    }
                }
            }
        }


        private static void GravarImprovements(CapacitatedVehicleRoutingProblem problem, Resultado item, DateTime agora, string name)
        {
            var fileNameImproves = string.Format("./{0}-{1}-{2}-{3}-{4}-{5}-{6}-{7}.txt", name, "Improves", agora.Year, agora.Month, agora.Day, agora.Hour, agora.Minute, agora.Second);

            using (FileStream fs = new FileStream(fileNameImproves, FileMode.Append, FileAccess.Write))
            using (StreamWriter sw = new StreamWriter(fs))
            {
                sw.WriteLine(string.Join(",", item.Improvements.Select(x => string.Format("({0}, {1})", x.Iteration, problem.NodeProvider.CountNodes()))));
            }
        }

        public static double TourDistance(IProblem problem, IEnumerable<IEnumerable<INode>> tours, INode depot)
        {
            double distance = 0;

            foreach (var item in tours)
            {
                distance += TourDistance(problem, item, depot);
            }

            return distance;
        }

        public static double TourDistance(IProblem problem, IEnumerable<INode> tour, INode depot)
        {
            double distance = problem.EdgeWeightsProvider.GetWeight(depot, tour.First());

            for (int i = 0; i + 1 < tour.Count(); i++)
            {
                INode first = i == 0 ? tour.First() : tour.ElementAt(i);
                INode second = tour.ElementAt(i + 1);
                double weight = problem.EdgeWeightsProvider.GetWeight(first, second);
                distance += weight;
            }

            distance += problem.EdgeWeightsProvider.GetWeight(tour.Last(), depot);

            return distance;
        }

        private static CapacitatedVehicleRoutingProblem FromTspFile(TspFile tspFile)
        {
            if (tspFile.Type != FileType.CVRP)
            {
                throw new ArgumentOutOfRangeException("tspFile");
            }

            TspFileDataExtractor extractor = new TspFileDataExtractor(tspFile);
            var nodeProvider = extractor.MakeNodeProvider();
            var nodes = nodeProvider.GetNodes();
            var edgeProvider = extractor.MakeEdgeProvider(nodes);
            var edgeWeightsProvider = extractor.MakeEdgeWeightsProvider();
            var fixedEdgesProvider = extractor.MakeFixedEdgesProvider(nodes);
            var depots = extractor.MakeDepotsProvider(nodes);
            var demand = extractor.MakeDemandProvider(nodes);
            return new CapacitatedVehicleRoutingProblem(tspFile.Name, tspFile.Comment, nodeProvider, edgeProvider, edgeWeightsProvider, fixedEdgesProvider, depots, demand);
        }
    }
}
