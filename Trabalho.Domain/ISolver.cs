﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TspLibNet.Graph.Nodes;

namespace Trabalho.Domain
{
    public interface ISolver
    {
        ISolution Solve();
    }
}
