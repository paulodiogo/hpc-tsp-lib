﻿using System;
namespace Trabalho.Domain
{
    public interface ISolution
    {
        TspLibNet.Graph.Nodes.INode Depot { get; }
        System.Collections.Generic.IEnumerable<System.Collections.Generic.IEnumerable<TspLibNet.Graph.Nodes.INode>> Tours { get; }
    }    
}
