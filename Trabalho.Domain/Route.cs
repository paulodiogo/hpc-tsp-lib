﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TspLibNet;
using TspLibNet.Graph.Nodes;

namespace Trabalho.Domain
{
    public class Route
    {
        private readonly List<INode> nodes;
        private readonly INode depot;
        private readonly CapacitatedVehicleRoutingProblem problem;

        public Route(CapacitatedVehicleRoutingProblem problem, List<INode> nodes, INode depot)
        {
            this.nodes = nodes;
            this.depot = depot;
            this.problem = problem;
        }

        public Route(CapacitatedVehicleRoutingProblem problem, INode node1, INode node2, INode depot)
        {
            this.nodes = new List<INode> { node1, node2 };
            this.depot = depot;
            this.problem = problem;
        }

        public void AddNode(int index, INode node)
        {
            this.nodes.Insert(index, node);
        }

        public void AddNodes(int index, IEnumerable<INode> nodes)
        {
            this.nodes.InsertRange(index, nodes);
        }

        public int IsNodeFirstOrLast(INode node)
        {
            var first = IsFirst(node);
            var last = IsLast(node);

            return first ? 0 : last ? nodes.Count : -1;
        }

        public bool IsFirst(INode node)
        {
            return nodes.First().Equals(node);
        }

        public bool IsLast(INode node)
        {
            return nodes.Last().Equals(node);
        }

        public bool IsInternal(INode node)
        {
            return this.nodes.Contains(node) && !(IsLast(node) || IsFirst(node));
        }

        public Dictionary<Change, Route> Neighbors
        {
            get
            {
                var neighbors = new Dictionary<Change, Route>();

                for (int consumer = 0; consumer <= this.nodes.Count() - 1; consumer++)
                {

                    for (int secondConsumer = consumer + 1; secondConsumer <= this.nodes.Count() - 1; secondConsumer++)
                    {
                        var route = new Route(this.problem, this.nodes.ToList(), depot);
                        route.Swap(consumer, secondConsumer);
                        neighbors.Add(new Change(consumer, secondConsumer), route);
                    }
                }

                return neighbors;
            }
        }

        public double TotalDemand
        {
            get
            {
                int totalDemand = 0;

                foreach (var item in nodes)
                {
                    totalDemand += this.problem.DemandProvider.GetDemand(item);
                }

                return totalDemand;
            }
        }

        public double TourDistance
        {
            get
            {
                double distance = this.problem.EdgeWeightsProvider.GetWeight(depot, nodes.First());

                for (int i = 0; i + 1 < nodes.Count(); i++)
                {
                    INode first = i == 0 ? nodes.First() : nodes.ElementAt(i);
                    INode second = nodes.ElementAt(i + 1);
                    double weight = problem.EdgeWeightsProvider.GetWeight(first, second);
                    distance += weight;
                }

                distance += problem.EdgeWeightsProvider.GetWeight(nodes.Last(), depot);

                return distance;
            }
        }

        public int Count
        {
            get
            {
                return this.nodes.Count;
            }
        }

        public void Swap(List<INode> nodes)
        {
            for (int i = 0; i < this.nodes.Count; i++)
            {
                this.nodes[i] = nodes[i];
            }
        }

        public void Swap(int firstDemand, int secondDemand)
        {
            INode temp = nodes[firstDemand];
            nodes[firstDemand] = nodes[secondDemand];
            nodes[secondDemand] = temp;
        }

        public IEnumerable<INode> Nodes
        {
            get { return nodes; }
        }

        public override string ToString()
        {
            return string.Join(" ,", this.nodes.Select(x => x.Id).ToArray()) + " Cost = " + TourDistance;
        }
    }

    
}
