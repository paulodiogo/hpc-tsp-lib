﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TspLibNet;
using TspLibNet.Graph.Nodes;

namespace Trabalho.Domain
{
    public class Economy
    {
        public Economy(INode i, INode j, double saving)
        {
            this.I = i;
            this.J = j;
            this.Saving = saving;
        }

        public INode I { get; private set; }
        public INode J { get; private set; }
        public double Saving { get; private set; }

        public override string ToString()
        {
            return string.Format("{0}:{1}={2}", this.I.Id, this.J.Id, this.Saving);
        }

        public static IEnumerable<Economy> CalculateEconomies(CapacitatedVehicleRoutingProblem problem, IEnumerable<INode> nodes, INode depot, int dimension)
        {
            var savings = new List<Economy>();
            for (int i = 1; i < dimension; i++)
            {
                for (int j = i; j < dimension; j++)
                {
                    if (i == j)
                        continue;

                    var nodeI = nodes.ElementAt(i);
                    var nodeJ = nodes.ElementAt(j);

                    var economy = (problem.EdgeWeightsProvider.GetWeight(nodeI, depot) +
                        problem.EdgeWeightsProvider.GetWeight(depot, nodeJ)) -
                        problem.EdgeWeightsProvider.GetWeight(nodeI, nodeJ);

                    savings.Add(new Economy(nodeI, nodeJ, economy));

                }
            }

            return savings.OrderByDescending(x => x.Saving).ToList();
        }
    }
}
