﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TspLibNet;
using TspLibNet.Graph.Nodes;
using TspLibNet.TSP;

namespace Trabalho.Domain
{
    public class ParallelClarkeWright : ISolver
    {
        private readonly TspFile tspFile;
        private readonly CapacitatedVehicleRoutingProblem problem;

        public ParallelClarkeWright(TspFile tspFile, CapacitatedVehicleRoutingProblem problem)
        {
            this.tspFile = tspFile;
            this.problem = problem;
        }


        public ISolution Solve()
        {
            var nodesDepot = problem.NodeProvider.GetNodes()
                .Where(x => problem.DemandProvider.GetDemand(x) == 0)
                .OrderBy(x => x.Id).ToList();

            var routes = new SolveNodes(this.tspFile, this.problem, nodesDepot.First());

            var economies = Economy.CalculateEconomies(problem, problem.NodeProvider.GetNodes(), nodesDepot.First(), tspFile.Dimension);

            foreach (var item in economies)
            {

                routes.ResolveRouteForNodes(item.I, item.J);

                if (routes.CountNodes == this.problem.NodeProvider.CountNodes() - 1)
                    break;
            }

            return new Solution(routes.RoutesNodes, nodesDepot.First());
        }

    }

    public class InvalidRouteException : Exception { }
}
