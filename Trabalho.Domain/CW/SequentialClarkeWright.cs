﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TspLibNet;
using TspLibNet.Graph.Nodes;
using TspLibNet.TSP;

namespace Trabalho.Domain
{
    public class SequentialClarkeWright : ISolver
    {
        private readonly TspFile tspFile;
        private readonly CapacitatedVehicleRoutingProblem problem;

        public SequentialClarkeWright(TspFile tspFile, CapacitatedVehicleRoutingProblem problem)
        {
            this.tspFile = tspFile;
            this.problem = problem;
        }


        public ISolution Solve()
        {
            var tour = new List<INode>();
            var tours = new List<List<INode>>();
            var totalDemands = 0;

            var nodesDepot = problem.NodeProvider.GetNodes()
                .Where(x => problem.DemandProvider.GetDemand(x) == 0)
                .OrderBy(x => x.Id).ToList();

            var economies = Economy.CalculateEconomies(problem, problem.NodeProvider.GetNodes(), nodesDepot.First(), tspFile.Dimension);

            while (tours.SelectMany(x => x).Count() < problem.NodeProvider.CountNodes() - 1)
            {

                var nodesOnRoutes = tours.SelectMany(y => y);

                var economiesWithNoNodeInRoute = economies.Where(x => !nodesOnRoutes.Contains(x.I) && !nodesOnRoutes.Contains(x.J)).ToList();

                foreach (var item in economiesWithNoNodeInRoute)
                {
                    //verifica se um dos nos da economia está em outra rota, caso um dos nodes esteja, a economia é ignorada
                    if (NodeOnOtherRoute(tours, item.I) || NodeOnOtherRoute(tours, item.J))
                        continue;

                    //verifica se ambos os nós já se encontram na rota corrente
                    if (NodeOnCurrentRoute(tour, item.I) && NodeOnCurrentRoute(tour, item.J))
                        continue;

                    var demandI = problem.DemandProvider.GetDemand(item.I);
                    var demandJ = problem.DemandProvider.GetDemand(item.J);

                    if (tour.Count == 0 && (demandI + demandJ) <= tspFile.Capacity)
                    {
                        totalDemands += (demandI + demandJ);
                        tour.Add(item.I);
                        tour.Add(item.J);
                    }
                    else if (tour.Count == 0 && (demandI + totalDemands) <= tspFile.Capacity)
                    {
                        totalDemands += demandI;
                        tour.Add(item.I);
                    }
                    else if (tour.Count == 0 && (demandJ + totalDemands) <= tspFile.Capacity)
                    {
                        totalDemands += demandJ;
                        tour.Add(item.J);
                    }
                    else if (tour.Count > 0)
                    {
                        var indexI = NodeIsFirstOrLast(tour, item.I);//0 or > 0 
                        var indexJ = NodeIsFirstOrLast(tour, item.J);//0 or > 0 

                        if (indexI >= 0 && (totalDemands + demandJ) <= tspFile.Capacity)
                        {
                            totalDemands += demandJ;
                            tour.Insert(indexI, item.J);
                        }
                        else if (indexJ >= 0 && (totalDemands + demandI) <= tspFile.Capacity)
                        {
                            totalDemands += demandI;
                            tour.Insert(indexJ, item.I);
                        }
                    }

                    var nodeCount = tours.SelectMany(x => x).Count();

                    if (tours.SelectMany(x => x).Count() == problem.NodeProvider.CountNodes() - 1)
                    {
                        break;
                    }
                }
                
                if (tour.Count == 0 && tours.SelectMany(x => x).Count() == problem.NodeProvider.CountNodes() - 2)
                {
                    var abandoned = problem.NodeProvider.GetNodes().Except(tours.SelectMany(x => x)).First();
                    tour.Add(abandoned);
                }

                var totalNodes = tours.SelectMany(x => x).Count();

                if (tour.Count == 0)
                    throw new InvalidRouteException();

                tours.Add(tour);
                tour = new List<INode>();
                totalDemands = 0;
            }


            return new Solution(tours, nodesDepot.First());
        }

        private int NodeIsFirstOrLast(List<INode> tour, INode item)
        {
            var first = tour.First().Equals(item);
            var last = tour.Last().Equals(item);


            return first ? 0 : last ? tour.Count - 1 : -1;
        }

        private bool NodeOnOtherRoute(List<List<INode>> tours, INode item)
        {
            return tours.SelectMany(x => x).Contains(item);
        }

        private bool NodeOnCurrentRoute(List<INode> tour, INode item)
        {
            return tour.Contains(item);
        }
    }    
}
