﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TspLibNet;
using TspLibNet.Graph.Nodes;
using TspLibNet.TSP;

namespace Trabalho.Domain
{
    public class TwoExchange : ISolver
    {
        private readonly TspFile tspFile;
        private readonly CapacitatedVehicleRoutingProblem problem;
        private readonly IEnumerable<IEnumerable<INode>> initialSolution;

        public TwoExchange(TspFile tspFile, CapacitatedVehicleRoutingProblem problem, IEnumerable<IEnumerable<INode>> initialSolution)
        {
            this.tspFile = tspFile;
            this.problem = problem;
            this.initialSolution = initialSolution;
        }


        public ISolution Solve()
        {

            bool improvement;
            double bestTourLength;
            int bestSwapIndex1 = 0;
            int bestSwapIndex2 = 0;

            var routes = new List<List<INode>>();

            var nodesDepot = problem.NodeProvider.GetNodes()
                .Where(x => problem.DemandProvider.GetDemand(x) == 0)
                .OrderBy(x => x.Id).ToList();

            foreach (var item in this.initialSolution)
            {
                var route = new Route(this.problem, item.ToList(), nodesDepot.First());

                do
                {
                    improvement = false;

                    for (int consumer = 0; consumer <= item.Count() - 1; consumer++)
                    {
                        bestTourLength = route.TourDistance;

                        for (int secondConsumer = consumer + 1; secondConsumer <= item.Count() - 1; secondConsumer++)
                        {
                            route.Swap(consumer, secondConsumer);

                            double length = route.TourDistance;

                            if (length < bestTourLength)
                            {
                                bestTourLength = length;
                                bestSwapIndex1 = consumer;
                                bestSwapIndex2 = secondConsumer;
                                improvement = true;
                            }

                            route.Swap(consumer, secondConsumer);

                        }

                        route.Swap(bestSwapIndex1, bestSwapIndex2);
                        bestSwapIndex1 = 0;
                        bestSwapIndex2 = 0;
                    }

                } while (improvement == true);

                routes.Add(route.Nodes.ToList());

            }

            return new Solution(routes, nodesDepot.First());
        }

    }
}
