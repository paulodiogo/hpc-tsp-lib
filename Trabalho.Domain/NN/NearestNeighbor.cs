﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TspLibNet;
using TspLibNet.Graph.Nodes;
using TspLibNet.TSP;

namespace Trabalho.Domain
{
    public class NearestNeighbor : ISolver
    {

        private readonly TspFile tspFile;
        private readonly CapacitatedVehicleRoutingProblem problem;

        public NearestNeighbor(TspFile tspFile, CapacitatedVehicleRoutingProblem problem)
        {
            this.tspFile = tspFile;
            this.problem = problem;
        }
                
        public ISolution Solve()
        {
            var tour = new List<INode>();
            var tours = new List<List<INode>>();
            var totalDemands = 0;

            var nodesDepot = problem.NodeProvider.GetNodes()
                .Where(x => problem.DemandProvider.GetDemand(x) == 0)
                .OrderBy(x => x.Id).ToList();

            var itemComparated = problem.NodeProvider.GetNodes()
                .Where(x => problem.DemandProvider.GetDemand(x) > 0)
                .OrderBy(x => x.Id).FirstOrDefault();

            totalDemands = problem.DemandProvider.GetDemand(itemComparated);
            tour.Add(itemComparated);


            while (totalDemands <= tspFile.Capacity || tours.SelectMany(x => x).Union(tour).Count() < problem.NodeProvider.CountNodes())
            {

                var minNode = problem.NodeProvider.GetNodes().Except(tours.SelectMany(x => x).Union(tour).Union(nodesDepot))
                    .OrderBy(x => problem.EdgeWeightsProvider.GetWeight(itemComparated, x)).FirstOrDefault();


                if (minNode != null)
                {
                    itemComparated = minNode;
                }
                else
                {
                    tours.Add(tour);
                    break;
                }

                var demandMinNode = problem.DemandProvider.GetDemand(minNode);

                if (totalDemands + demandMinNode <= tspFile.Capacity)
                {
                    totalDemands += demandMinNode;
                    tour.Add(minNode);
                }
                else
                {
                    tours.Add(tour);
                    totalDemands = demandMinNode;
                    tour = new List<INode> { minNode };
                }
            }

            return new Solution(tours, nodesDepot.First());
        }
    }
}
