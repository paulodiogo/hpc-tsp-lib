﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TspLibNet;
using TspLibNet.Graph.Nodes;
using TspLibNet.TSP;

namespace Trabalho.Domain
{
    public class SolveNodes
    {
        private readonly List<Route> routes;
        private readonly CapacitatedVehicleRoutingProblem problem;
        private readonly TspFile tspFile;
        private readonly INode depot;

        public SolveNodes(TspFile tspFile, CapacitatedVehicleRoutingProblem problem, INode depot)
        {
            this.routes = new List<Route>();
            this.problem = problem;
            this.tspFile = tspFile;
            this.depot = depot;
        }

        public void ResolveRouteForNodes(INode node1, INode node2)
        {
            var demandI = problem.DemandProvider.GetDemand(node1);
            var demandJ = problem.DemandProvider.GetDemand(node2);

            if (!this.routes.Any(x => x.Nodes.Contains(node1)) && !this.routes.Any(x => x.Nodes.Contains(node2)))
            {
                var route = new Route(this.problem, node1, node2, this.depot);
                this.routes.Add(route);
            }
            else
            {
                if (this.routes.Any(x => x.IsInternal(node1)) || this.routes.Any(x => x.IsInternal(node2)))
                    return;

                var stateNode1 = this.routes.FirstOrDefault(x => x.IsNodeFirstOrLast(node1) >= 0);
                var stateNode2 = this.routes.FirstOrDefault(x => x.IsNodeFirstOrLast(node2) >= 0);

                if (stateNode1 != null && stateNode2 == null && stateNode1.TotalDemand + demandJ <= this.tspFile.Capacity && !stateNode1.Nodes.Contains(node2))
                {

                    if (stateNode1.IsInternal(node1))
                        return;

                    stateNode1.AddNode(stateNode1.IsNodeFirstOrLast(node1), node2);

                    return;
                }
                else if (stateNode1 == null && stateNode2 != null && stateNode2.TotalDemand + demandI <= this.tspFile.Capacity && !stateNode2.Nodes.Contains(node1))
                {
                    if (stateNode2.IsInternal(node2))
                        return;

                    stateNode2.AddNode(stateNode2.IsNodeFirstOrLast(node2), node1);

                    return;
                }
                else if (stateNode1 != null && stateNode2 != null && stateNode1.TotalDemand + stateNode2.TotalDemand <= this.tspFile.Capacity)
                {
                    this.routes.Remove(stateNode1);
                    this.routes.Remove(stateNode2);

                    var posicaoNode1 = stateNode1.IsNodeFirstOrLast(node1);
                    var posicaoNode2 = stateNode1.IsNodeFirstOrLast(node2);

                    if (stateNode1.IsFirst(node1) && stateNode2.IsFirst(node2))
                    {
                        var invertedSortRoute2 = stateNode2.Nodes.Reverse();

                        var route = new Route(this.problem, invertedSortRoute2.Union(stateNode1.Nodes).ToList(), this.depot);
                        this.routes.Add(route);
                        return;
                    }
                    else if (stateNode1.IsLast(node1) && stateNode2.IsLast(node2))
                    {
                        var invertedSortRoute2 = stateNode2.Nodes.Reverse();

                        var route = new Route(this.problem, stateNode1.Nodes.Union(invertedSortRoute2).ToList(), this.depot);
                        this.routes.Add(route);
                        return;
                    }
                    else if (stateNode1.IsLast(node1) && stateNode2.IsFirst(node2))
                    {
                        var route = new Route(this.problem, stateNode1.Nodes.Union(stateNode2.Nodes).ToList(), this.depot);
                        this.routes.Add(route);
                        return;
                    }
                    else if (stateNode1.IsFirst(node1) && stateNode2.IsLast(node2))
                    {
                        var route = new Route(this.problem, stateNode2.Nodes.Union(stateNode1.Nodes).ToList(), this.depot);
                        this.routes.Add(route);
                        return;
                    }

                }
            }
        }


        public int CountNodes
        {
            get
            {
                return this.routes.Sum(x => x.Count);
            }
        }

        public IEnumerable<IEnumerable<INode>> RoutesNodes
        {
            get
            {
                return this.routes.Select(x => x.Nodes);
            }
        }

        public override string ToString()
        {
            return string.Join(";", this.routes.Select(x => x.ToString()).ToArray());
        }

    }
}
