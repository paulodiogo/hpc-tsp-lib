﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trabalho.Domain
{
    public class TabuList : List<Trabalho.Domain.TabuList.ItemTabuComVida>
    {
        public void Add(int i, int j, int n)
        {
            this.Add(new ItemTabuComVida(i, j, n));
        }

        public bool IsPairTabu(int i, int j)
        {
            return this.Any(x => (x.X == i && x.Y == j) || (x.X == j && x.Y == i));
        }
        public void UpdateLifes()
        {
            foreach (var item in this)
            {
                item.DecreaseLife();
            }

            this.RemoveAll(x => x.IsExpired());
        }

        public class ItemTabuComVida
        {
            private int MAX_ITERATIONS_FACTOR = 100;

            private readonly int x;
            private readonly int y;
            private int life;
            private bool isNew = false;

            public ItemTabuComVida(int x, int y, int n)
            {
                this.x = x;
                this.y = y;
                this.life = MAX_ITERATIONS_FACTOR;
                this.isNew = true;
            }

            public void DecreaseLife()
            {
                if (this.isNew)
                {
                    this.isNew = false;
                }
                else
                {
                    life--;
                }
            }

            public bool IsExpired()
            {
                return this.life < 0;
            }

            public int X { get { return x; } }
            public int Y { get { return y; } }

        }
    }
}
