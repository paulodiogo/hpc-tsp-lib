﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TspLibNet.Graph.Nodes;

namespace Trabalho.Domain
{
    public class SolutionTabu : Solution, ISolutionTabu
    {

        public SolutionTabu(IEnumerable<IEnumerable<INode>> tours, INode depot, IEnumerable<Improvement> improvements)
            : base(tours, depot)
        {
            this.Improvements = improvements;
        }

        public IEnumerable<Improvement> Improvements { get; private set; }
    }
}
