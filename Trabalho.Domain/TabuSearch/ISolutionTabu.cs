﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trabalho.Domain
{
    public interface ISolutionTabu : ISolution
    {
        System.Collections.Generic.IEnumerable<Improvement> Improvements { get; }
    }
}
