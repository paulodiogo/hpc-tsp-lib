﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TspLibNet;
using TspLibNet.Graph.Nodes;
using TspLibNet.TSP;

namespace Trabalho.Domain
{
    public class TabuSearch : ISolver
    {

        private readonly int MAX_ITERATIONS = 5000;
        private readonly TspFile tspFile;
        private readonly CapacitatedVehicleRoutingProblem problem;
        private readonly IEnumerable<IEnumerable<INode>> initialSolution;


        public TabuSearch(TspFile tspFile, CapacitatedVehicleRoutingProblem problem, IEnumerable<IEnumerable<INode>> initialSolution)
        {
            this.tspFile = tspFile;
            this.problem = problem;
            this.initialSolution = initialSolution;
        }

        public ISolution Solve()
        {
            var routes = new List<List<INode>>();
            var improvements = new List<Improvement>();

            var depot = problem.NodeProvider.GetNodes()
                            .First(x => problem.DemandProvider.GetDemand(x) == 0);

            var bestSolution = new SolutionTabuSearch(problem, initialSolution, depot);

            var iteration = 0;
            var tabuList = new TabuList();

            foreach (var item in this.initialSolution)
            {
                var route = new Route(this.problem, item.ToList(), depot);
                var currentRoute = new Route(this.problem, item.ToList(), depot);

                do
                {

                    if (item.Count() <= 2)
                        break;

                    var candidates = currentRoute.Neighbors.OrderBy(x => x.Value.TourDistance).ToList();
                    var bestCandidates = candidates.Where(x => !tabuList.IsPairTabu(x.Key.X, x.Key.Y));

                    if (bestCandidates.Any())
                    {
                        var bestCandidate = bestCandidates.OrderBy(x => x.Value.TourDistance).First();

                        if (route.TourDistance > bestCandidate.Value.TourDistance)
                        {
                            route.Swap(bestCandidate.Value.Nodes.ToList());
                            improvements.Add(new Improvement(iteration));
                        }

                        tabuList.Add(bestCandidate.Key.X, bestCandidate.Key.Y, problem.EdgeProvider.CountEdges());
                        currentRoute.Swap(bestCandidate.Key.X, bestCandidate.Key.Y);
                    }
                    tabuList.UpdateLifes();

                    iteration++;
                }
                while (iteration <= MAX_ITERATIONS);

                routes.Add(route.Nodes.ToList());
            }
            return new SolutionTabu(routes, depot, improvements);
        }
    }
}
