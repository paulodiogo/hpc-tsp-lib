﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TspLibNet;
using TspLibNet.Graph.Nodes;

namespace Trabalho.Domain
{
    internal class SolutionTabuSearch
    {

        private readonly IProblem problem;
        private readonly INode depot;
        private IEnumerable<IEnumerable<INode>> tours;

        public SolutionTabuSearch(IProblem problem, IEnumerable<IEnumerable<INode>> tours, INode depot)
        {
            this.problem = problem;
            this.tours = tours;
            this.depot = depot;
        }

        public void ChangeBestTours(IEnumerable<IEnumerable<INode>> tours)
        {
            this.tours = tours;
        }

        public double SolutionDistance()
        {
            double distance = 0;

            foreach (var item in tours)
            {
                distance += this.TourDistance(item);
            }

            return distance;
        }

        private double TourDistance(IEnumerable<INode> tour)
        {
            double distance = this.problem.EdgeWeightsProvider.GetWeight(this.depot, tour.First());

            for (int i = 0; i + 1 < tour.Count(); i++)
            {
                INode first = i == 0 ? tour.First() : tour.ElementAt(i);
                INode second = tour.ElementAt(i + 1);
                double weight = problem.EdgeWeightsProvider.GetWeight(first, second);
                distance += weight;
            }

            distance += problem.EdgeWeightsProvider.GetWeight(tour.Last(), depot);

            return distance;
        }

        public IEnumerable<IEnumerable<INode>> Routes { get { return this.tours; } }
    }
}
