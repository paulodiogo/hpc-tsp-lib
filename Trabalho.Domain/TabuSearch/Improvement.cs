﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trabalho.Domain
{
    public class Improvement
    {

        public Improvement(int iteration)
        {
            this.Time = DateTime.Now;
            this.Iteration = iteration;
        }

        public DateTime Time { get; private set; }
        public int Iteration { get; private set; }
        
    }
}
