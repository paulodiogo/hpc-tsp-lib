﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TspLibNet.Graph.Nodes;

namespace Trabalho.Domain
{
    public class Solution : Trabalho.Domain.ISolution
    {

        public Solution(IEnumerable<IEnumerable<INode>> tours, INode depot)
        {
            this.Tours = tours;
            this.Depot = depot;
        }

        public IEnumerable<IEnumerable<INode>> Tours { get; private set; }
        public INode Depot { get; private set; }
    }    
}
